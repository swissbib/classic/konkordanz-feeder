import Dependencies._

ThisBuild / scalaVersion := "2.13.3"
ThisBuild / organization := "org.swissbib"
ThisBuild / organizationName := "swissbib"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") Some(tag)
  else None
}


lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)

  .settings(
    name := "konkordanz-feeder",
    assemblyJarName in assembly := "app.jar",
    mainClass in assembly := Some("ch.swissbib.Main"),
    //resolvers += "kafka-metadata-wrapper" at "https://gitlab.com/api/v4/projects/11507450/packages/maven",
    //resolvers ++= Seq(
    //  "Memobase Utils" at "https://dl.bintray.com/memoriav/memobase"
    //),
    assemblyMergeStrategy in assembly := {
      case "log4j.properties" => MergeStrategy.first
      case "log4j2.xml" => MergeStrategy.first
      case "module-info.class" => MergeStrategy.discard
      case x =>
        val oldStrategy = (assemblyMergeStrategy in assembly).value
        oldStrategy(x)
    },

    libraryDependencies ++= Seq(
      mongoJavaClientCurrent,

      scala_xml_module,
      log4jApi,
      log4jCore,
      log4jScala,
      scalaMock % Test,
      //kafkaStreamsTestUtils % Test,
      scalaTest % Test)
  )
