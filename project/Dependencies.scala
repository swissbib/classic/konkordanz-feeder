import sbt._

object Dependencies {

  lazy val log4jV = "2.12.0"
  lazy val log4jscalaV  = "12.0"



  lazy val mongoJavaClientCurrent = "org.mongodb" % "mongodb-driver-sync" % "4.2.1"

  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.1.2"

  lazy val scalaMock = "org.scalamock" %% "scalamock" % "5.0.0"

  lazy val scala_xml_module = "org.scala-lang.modules" %% "scala-xml" % "1.3.0"
  lazy val log4jApi = "org.apache.logging.log4j" % "log4j-api" % log4jV
  lazy val log4jCore = "org.apache.logging.log4j" % "log4j-core" % log4jV
  lazy val log4jScala = "org.apache.logging.log4j" %% "log4j-api-scala" % log4jscalaV
  lazy val log4jSlf4j = "org.apache.logging.log4j" % "log4j-slf4j-impl" % log4jV



}
