package ch.swissbib

import java.util.Properties

object Main {

  val props = new Properties()
//  props.put("mongoUri","mongodb://localhost:29017")
  //mongodb://[username[:password]@]host1[:port1][,host2[:port2],...[,hostN[:portN]]][/[database]][?options]
//  props.put("mongoDb","konkordanz_swissbib_slsp")

  props.put("mongoUri", getEnv("MONGO_URI"))
  props.put("mongoDb", getEnv("MONGO_DB"))

  val dp = new DocumentProcessor(props)

  dp.ladeKonkordanzen()



  def main(args: Array[String]): Unit = {

  }


  def getEnv(value: String): String = {
    val envValue: Option[String] = sys.env.get(value)
    if (envValue.isDefined) {
      envValue.get
    } else {
      Option(System.getProperties.get(value)) match {
        case Some(value) => value.toString
        case None => throw new Exception("Environment variable " + value + " not available")
      }
    }
  }



}
