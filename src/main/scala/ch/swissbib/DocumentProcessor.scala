/*
 * mongo-feeder (swisscollections)
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.swissbib

import com.mongodb.BasicDBObject
import com.mongodb.client.FindIterable
import com.mongodb.client.model.{InsertManyOptions, UpdateOptions}

import java.util.Properties
import org.bson.Document

import java.time.LocalDate
import scala.util.{Failure, Success, Try}
import scala.xml.{Elem, XML}
import com.mongodb.client.result.{InsertManyResult, InsertOneResult, UpdateResult}
import org.apache.logging.log4j.scala.Logging

import java.io.{BufferedReader, InputStream, InputStreamReader}
import java.util
import java.util.zip.GZIPInputStream
import scala.collection.mutable




class DocumentProcessor (properties: Properties) extends Logging{

  private val collectionWrapper = MongoJavaCollectionWrapper(properties)

  private case class BatchedDocument(source: String, record: String, deleted: Boolean = false)

  private val batchBuffer: scala.collection.mutable.HashMap[String, BatchedDocument] = mutable.HashMap.empty


  def ladeKonkordanzen(): Unit = {

    class BufferedReaderIterator(reader: BufferedReader) extends Iterator[String] {
      override def hasNext: Boolean = reader.ready
      override def next: String = reader.readLine()
    }



    val resource: InputStream = getClass.getResourceAsStream("/Konkordanz_sbBB_url_swisscovery-NZ.csv.gz")

    val reader = new BufferedReaderIterator(
      new BufferedReader(
        new InputStreamReader(
          new GZIPInputStream(resource)
        )))


    var list: java.util.List[Document] = new util.ArrayList[Document]()

    while (reader.hasNext) {
      val splittedLine = reader.next.split(";")


      //println(splittedLine(0))
      val uri = splittedLine(1)
      val id = uri.substring(uri.lastIndexOf("/") + 1)
      //println(id)

      if (isDocNotAvailable(id)) insertOne(id, uri )

      /*
      list.add( new Document("_id",id)
        .append("target",splittedLine(1)))

      if (list.size() % 100 == 0) {
        updateMany(list)
        list = new util.ArrayList[Document]()
      }

       */

    }

  }


  def updateMany(list: java.util.List[Document]): Unit = {
    Try[InsertManyResult] {
      val imo = new InsertManyOptions
      imo.ordered(false)

      collectionWrapper.collectionMap("konkordanz").insertMany(list,imo)
    } match {
      case Success(_) =>
        //println(value)
        //in case we want to implement reporting we should probably use this value
        logger.info(s"(bunch of records were upserted")

      case Failure(exception) =>
        logger.error(s"error trying to upsert bunch of docs ${exception.getMessage}")
        //throw exception
    }

  }

  def isDocNotAvailable(id: String): Boolean = {

    val bdbo = new BasicDBObject()
    //val source = elem._2.source
    //val sourceUpper = elem._2.source.toUpperCase
    //val key = elem._1
    //new we do not need to create Prefix because this is done by ingester
    //bdbo.put("_id",  s"(${elem._2.source.toUpperCase})${elem._1}")
    bdbo.put("_id",  id)
    val doc: FindIterable[Document] = collectionWrapper.collectionMap("konkordanz").find(bdbo)

    Option(doc.first()) match {
      case Some(_) =>
        false
      case None =>
          true

    }

  }

  def insertOne(id: String, uri: String): Unit = {
    Try[InsertOneResult] {
      collectionWrapper.collectionMap("konkordanz").insertOne(
        new Document("_id",id)
          .append("target",uri)
      )
    } match {
      case Success(_) =>
        //println(value)
        //in case we want to implement reporting we should probably use this value
        logger.info(s"(document ${id} was inserted")

      case Failure(exception) =>
        logger.error(s"error trying to insert document ${id} / ${exception.getMessage}")
        //throw exception
    }

  }


}

sealed trait BatchProcessResult extends Serializable with Product
case object Committed extends BatchProcessResult
case object Continue extends BatchProcessResult
case class ErrorInBatch(value: Throwable) extends BatchProcessResult

