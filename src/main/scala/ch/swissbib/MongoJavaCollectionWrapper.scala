/*
 * mongo-feeder (swisscollections)
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package ch.swissbib


import com.mongodb.client.MongoClients

import java.util.Properties
//import scala.jdk.CollectionConverters._
import com.mongodb.client.MongoCollection
import org.bson.Document





class MongoJavaCollectionWrapper(val collectionMap: Map[String, MongoCollection[Document]])

object MongoJavaCollectionWrapper {

  def apply (properties: Properties): MongoJavaCollectionWrapper = {

    //old client API
    //val mongoUri: MongoClientURI = new MongoClientURI(properties.getProperty("mongoUri"))
    //val mongoJavaClient = new MongoClient(mongoUri)

    //new Client API - version 4.x
    //https://mongodb.github.io/mongo-java-driver/4.2/driver/getting-started/quick-start/
    val mongoJavaClient = MongoClients.create(properties.getProperty("mongoUri"));

    val konkordanzDB = mongoJavaClient.getDatabase(properties.getProperty("mongoDb"))

    val konkordanzCollection = Map("konkordanz" -> konkordanzDB.getCollection("konkordanz"))

    //todo find a possibility how to check if we have a valid connection???

    new MongoJavaCollectionWrapper(konkordanzCollection)
  }




}
